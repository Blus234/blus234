
Zadania - bazy danych:
1# Konfiguracja środowiska
- zainstaluj pakiet xampp 
https://www.apachefriends.org/xampp-files/7.2.28/xampp-windows-x64-7.2.28-0-VC15-installer.exe
2# Skonfiguruj sobie środowisko git
- zainstaluj 
https://www.gitkraken.com/download/windows64
(lub alternatywnie podobnego klietna UI)
- po instalacji najzwyczajniej sklonuj repozytorium z bitbucketa tj skopiuj link z bucketa i wklej go w sekcji url
w zakładce clone
- wszelkie zmiany zapisuj do katalogu w którym się znajduje repozytorium
3# Odpalanie panelu zarządzania bazą danych
- po zainstalowaniu xamppa uruchamiasz serwer apache oraz mysql
- w momencie gdy będzie uruchomiony (stan usługi na zielono) klikasz button admin
- tworzysz bazę danych z odpowiednim kodowaniem znaków

4# Import bazy
- do utworzonej bazy danych importujecie zewnętrzny plik:
http://mages.pl/zscku/baza.sql

Następnie wykonujecie poniższe kwerendy (wykonując zrzut ekranu każdego z zapytań)
Zapytanie 1: wybierające jedynie pola id, tytul i tresc z tabeli ogloszenie dla tych rekordów, dla których kategoria to książki
Zapytanie 2: wykorzystujące relację pomiędzy tabelami i wybierające jedynie telefon tego użytkownika, który jest przypisany do ogłoszenia numer 1
Zapytanie 3: tworzące użytkownika moderator na localhost z hasłem qwerty
Zapytanie 4: nadające prawa dla użytkownika moderator do usuwania i przeglądania danych w tabeli ogloszenie.

=== DEADLINE 27/03/20 ===

W razie pytań / wątpliwości dajcie znać, wtedy omówię to na konfie.
Pozdrawiam,
MJ